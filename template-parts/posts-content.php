<?php 
global $_AMB_PREFIX;
global $_AMB_estiloCaos;
global $_i;
global $tamanhoDoCaos;
global $cos,$sin;

$amb_content = get_the_content();

$_AMB_estiloCaos = "transform: translate({$cos}%,{$sin}%); z-index: $_i";

echo "\n<div class='$_AMB_PREFIX-post caos' style='".$_AMB_estiloCaos."'>\n";
echo '<div class="'.$_AMB_PREFIX.'-post-title">'.get_the_title().'</div>';
echo '<div class="'.$_AMB_PREFIX.'-post-content">';
echo $amb_content;
echo '</div>';
echo "</div>\n";

$angle =  ($_i * pi() / ($tamanhoDoCaos/2));
$sin = sin($angle) * 100;
$cos = cos($angle) * 100 + 70;
