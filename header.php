<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>

	<head>
        <title><?php
        if(is_front_page() || is_home()){
            echo get_bloginfo('name');
        } else{
            echo wp_title('');
        }  ?></title>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" >
		<link rel="profile" href="https://gmpg.org/xfn/11">

		<?php wp_head(); ?>

	</head>

	<body <?php body_class(); ?>>

		<?php
		wp_body_open();
		?>

		<header id="site-header" class="header-footer-group" role="banner">
			<div class="header-inner section-inner">
				<div class="header-titles-wrapper">
                </div>
            </div>
        </header>
