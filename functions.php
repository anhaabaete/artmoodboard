<?php

$_AMB_PREFIX = 'ArtMoodBoard';
$_AMBn = "\n";
$amb_theme = wp_get_theme();
$_AMB_estiloCaos = '';
$_i = 0;
$cos=0;
$sin=0;

// Constroi o caos
$AMB_post = wp_count_posts(); // Pega dados dos posts
$tamanhoDoCaos = $AMB_post->publish; //Pega quantidade de posts
$seq_post = range(0, $tamanhoDoCaos-1); //Cria um uma sequencia de todos os posts
shuffle($seq_post); // fator caótico provido da função rand do cpu
$_AMB_CAOS = array_slice($seq_post, 0, $tamanhoDoCaos); //organiza o caos para ser usado



//Load alls scripts
function artmoodboard_load_scripts() {
    global $amb_theme;

    wp_enqueue_style( 'amb-style', 
        get_template_directory_uri().'/assets/css/main.css',array(),
        rand(10,100));

    wp_enqueue_script( 'amb-script', 
        get_template_directory_uri().'/assets/js/main.js', array('jquery'), 
        rand(10,100));
}

add_action( 'wp_enqueue_scripts', 'artmoodboard_load_scripts');


function amb_custom_background() { 
    $another_args = array(
        'default-color'      => '0000ff',
        'default-image'      => ''     
    );
    add_theme_support( 'custom-background', $another_args );
}

add_action('after_setup_theme','amb_custom_background');

